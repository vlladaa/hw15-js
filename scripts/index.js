
//теорія
//рекурсія - виклик функції у самій собі допоки не буде виконуватись умова її припинення.
//рекурсія використовується там де є операції які повторюються 

let number = "";
do { number = prompt('Введіть число', `${number}`); }
while (number === null || number === "" || isNaN(number))

//варіант 1
// const calcFactorial = () => {

//     let count = 1;
//     for (let i = 1; i <= number; i++) {
//         count *= i;
//     }
//     return count;
// }

//варіант 2 (рекурсія)
const calcFactorial = (num) => {

    if (num <= 1) {
        return 1;
    }
    if (num > 1) {
        return num*(calcFactorial(num - 1))
    }
}

console.log(calcFactorial(+number))